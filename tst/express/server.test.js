import {expect} from 'chai';
import {
  methods,
  RouteBuilder,
  Server,
  CustomError,
  HttpError,
} from '../../src';
import supertest from 'supertest';
import joi from 'joi';

describe('Server unit tests,', () => {
  let server;
  beforeEach(() => {
    server = new Server();

    const route = new RouteBuilder()
        .setPath('/')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('working');
        })
        .build();

    server.addRoutes(route);
  });

  test('will pass on single route with no input validator', async () => {
    server.buildMiddleware();
    await supertest(server.app)
        .get('/')
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal('working');
        }));
  });

  test('will throw 404 for bad path', async () => {
    server.buildMiddleware();
    await supertest(server.app)
        .get('/badPath')
        .expect(404)
        .then(((res) => {
          expect(res.text).to.be.equal(
              `{"status":404,"message":"NotFoundError: Not Found"}`);
        }));
  });

  test('success on /ping route', async () => {
    server.buildMiddleware();
    await supertest(server.app)
        .get('/ping')
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`healthy`);
        }));
  });

  function singleInputValidationRouteSetup() {
    const parmaValidator = joi.object({
      id: joi.string().min(6).required(),
    });

    const route = new RouteBuilder()
        .setPath('/user/:id')
        .setMethod(methods.GET)
        .setParamsValidator(parmaValidator)
        .addController((req, res) => {
          res.send(req.params.id);
        })
        .build();
    server.addRoutes(route);
    server.buildMiddleware();
  }

  test('will pass on single route with input validators', async () => {
    singleInputValidationRouteSetup();
    await supertest(server.app)
        .get('/user/arzooo')
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal('arzooo');
        }));
  });

  // eslint-disable-next-line max-len
  test('will throw 400 with invalid input for single route with input validators.', async () => {
    singleInputValidationRouteSetup();
    const expected =
      // eslint-disable-next-line max-len
      `Error validating request params. "id" length must be at least 6 characters long.`;
    await supertest(server.app)
        .get('/user/arzoo')
        .expect(400)
        .then(((res) => {
          expect(res.text).to.be.equal(expected);
        }));
  });

  function multipleInputValidationWithSingleControllerSetup() {
    const paramsValidator = joi.object({
      id: joi.string().required(),
    });

    const bodyValidator = joi.object({
      name: joi.string().min(3).required(),
      age: joi.number().less(15).required(),
    });

    const route = new RouteBuilder()
        .setPath('/user/:id')
        .setMethod(methods.POST)
        .setParamsValidator(paramsValidator)
        .setBodyValidator(bodyValidator)
        .addController((req, res) => {
          res.send({
            id: req.params.id,
            name: req.body.name,
            age: req.body.age,
          });
        })
        .build();
    server.addRoutes(route);
    server.buildMiddleware();
  }

  test('will return 200 for valid input for multiple input validators.',
      async () => {
        multipleInputValidationWithSingleControllerSetup();
        await supertest(server.app)
            .post('/user/1')
            .send({
              name: 'Aang',
              age: 12,
            })
            .expect(200)
            .then(((res) => {
              expect(res.text).to.be.equal(`{"id":"1","name":"Aang","age":12}`);
            }));
      });

  test('will throw 400 for invalid age with multiple input validators.',
      async () => {
        multipleInputValidationWithSingleControllerSetup();
        await supertest(server.app)
            .post('/user/1')
            .send({
              name: 'Aang',
              age: 112,
            })
            .expect(400)
            .then(((res) => {
              expect(res.text).to.be.equal(
                  `Error validating request body. "age" must be less than 15.`);
            }));
      });

  test('will throw 400 for missing age with multiple input validators.',
      async () => {
        multipleInputValidationWithSingleControllerSetup();
        await supertest(server.app)
            .post('/user/1')
            .send({
              name: 'Aang',
            })
            .expect(400)
            .then(((res) => {
              expect(res.text).to.be.equal(
                  `Error validating request body. "age" is required.`);
            }));
      });

  test('will throw 400 for missing name with multiple input validators.',
      async () => {
        multipleInputValidationWithSingleControllerSetup();
        await supertest(server.app)
            .post('/user/1')
            .send({
              age: 12,
            })
            .expect(400)
            .then(((res) => {
              expect(res.text).to.be.equal(
                  `Error validating request body. "name" is required.`);
            }));
      });

  test('will throw 400 for missing name & age with multiple input validators.',
      async () => {
        multipleInputValidationWithSingleControllerSetup();
        const expected =
          // eslint-disable-next-line max-len
          `Error validating request body. "name" is required. "age" is required.`;
        await supertest(server.app)
            .post('/user/1')
            .send({})
            .expect(400)
            .then(((res) => {
              expect(res.text).to.be.equal(expected);
            }));
      });

  function multipleInputValidationWithMultipleContorollerSetup() {
    const paramsValidator = joi.object({
      id: joi.string().required(),
    });

    const bodyValidator = joi.object({
      name: joi.string().min(3).required(),
      age: joi.number().less(15).required(),
      throwExcepton: joi.boolean(),
      errorCode: joi.number(),
    });

    const route1 = (req, res, next) => {
      if (req.body.throwExcepton) {
        if (req.body.errorCode) {
          throw new HttpError(
              'Unit test infused error with status code', req.body.errorCode);
        }

        throw new CustomError('Unit test infused error');
      }

      req.city = 'Patola Mountains';
      next();
    };

    const route2 = (req, res) => {
      const city = req.city;
      res.send({
        id: req.params.id,
        name: req.body.name,
        age: req.body.age,
        city: city,
      });
    };

    const route = new RouteBuilder()
        .setPath('/user/:id')
        .setMethod(methods.POST)
        .setParamsValidator(paramsValidator)
        .setBodyValidator(bodyValidator)
        .addController(route1)
        .addController(route2)
        .build();

    server.addRoutes(route);
    server.buildMiddleware();
  }

  test('will return 200 for multiple input validators & controllers.',
      async () => {
        multipleInputValidationWithMultipleContorollerSetup();
        const expected =
          `{"id":"1","name":"Aang","age":12,"city":"Patola Mountains"}`;
        await supertest(server.app)
            .post('/user/1')
            .send({
              name: 'Aang',
              age: 12,
            })
            .expect(200)
            .then(((res) => {
              expect(res.text).to.be.equal(expected);
            }));
      });

  test('will return 500 on throw exception validators & multiple controllers.',
      async () => {
        multipleInputValidationWithMultipleContorollerSetup();
        const expected =
          `{"status":500,"message":"CustomError: Unit test infused error"}`;
        await supertest(server.app)
            .post('/user/1')
            .send({
              name: 'Aang',
              age: 12,
              throwExcepton: true,
            })
            .expect(500)
            .then(((res) => {
              expect(res.text).to.be.equal(expected);
            }));
      });

  test('will return 300 on throwing exception with custom error code.',
      async () => {
        multipleInputValidationWithMultipleContorollerSetup();
        const expected =
          // eslint-disable-next-line max-len
          `{"status":300,"message":"CustomError: Unit test infused error with status code"}`;
        await supertest(server.app)
            .post('/user/1')
            .send({
              name: 'Aang',
              age: 12,
              throwExcepton: true,
              errorCode: 300,
            })
            .expect(300)
            .then(((res) => {
              expect(res.text).to.be.equal(expected);
            }));
      });

  function queryInputValidationRouteSetup() {
    const QueryValidator = joi.object({
      name: joi.string().required(),
    });

    const route = new RouteBuilder()
        .setPath('/user')
        .setMethod(methods.GET)
        .setQueryValidator(QueryValidator)
        .addController((req, res) => {
          res.send(req.query.name);
        })
        .build();
    server.addRoutes(route);
    server.buildMiddleware();
  }

  test('will return 200 for valid query value.', async () => {
    queryInputValidationRouteSetup();
    await supertest(server.app)
        .get(`/user?name=Toph_Beifong`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Toph_Beifong`);
        }));
  });

  test('will return 400 for missing query value.', async () => {
    queryInputValidationRouteSetup();
    const expected =
      `Error validating request query. "name" is not allowed to be empty.`;
    await supertest(server.app)
        .get(`/user?name=`)
        .expect(400)
        .then(((res) => {
          expect(res.text).to.be.equal(expected);
        }));
  });

  function headerInputValidationRouteSetup() {
    const headerValidator = joi.object({
      name: joi.string().required(),
    });

    const route = new RouteBuilder()
        .setPath('/user')
        .setMethod(methods.GET)
        .setHeadersValidator(headerValidator)
        .addController((req, res) => {
          res.send(req.originalHeaders.name);
        })
        .build();
    server.addRoutes(route);
    server.buildMiddleware();
  }

  test('will return 200 for valid query value.', async () => {
    headerInputValidationRouteSetup();
    await supertest(server.app)
        .get(`/user`)
        .set('name', 'katara')
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`katara`);
        }));
  });

  test('will return 400 for missing header value.', async () => {
    headerInputValidationRouteSetup();
    await supertest(server.app)
        .get(`/user`)
        .expect(400)
        .then(((res) => {
          expect(res.text).to.be.equal(
              `Error validating request headers. "name" is required.`);
        }));
  });

  function preHandlerRouterSetup() {
    const route = new RouteBuilder()
        .setPath('/user')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Earth, Fire, Water, Air');
        })
        .build();

    server.addPreHandlers(function(req, res, next) {
      res.setHeader('name', 'Sokka');
      next();
    });
    server.addRoutes(route);
    server.buildMiddleware();
  }

  test('will return header `name` with value `Sokka`', async () => {
    preHandlerRouterSetup();
    await supertest(server.app)
        .get(`/user`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Earth, Fire, Water, Air`);
          expect(res.headers.name).to.be.equal(`Sokka`);
        }));

    await supertest(server.app)
        .get(`/`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`working`);
          expect(res.headers.name).to.be.equal(`Sokka`);
        }));
  });

  function postHandlerRouterSetup() {
    server.temp = 'Bumi';

    const route = new RouteBuilder()
        .setPath('/user')
        .setMethod(methods.GET)
        .addController((req, res, next) => {
          res.send('Earth, Fire, Water, Air');
          next();
        })
        .build();

    server.addPostHandlers(function(req, res, next) {
      server.temp = 'Zuko';
    });
    server.addRoutes(route);
    server.buildMiddleware();
  }


  test('will call post handler and set value', async () => {
    postHandlerRouterSetup();
    await supertest(server.app)
        .get(`/user`)
        .expect(200)
        .then(((res) => {
          expect(server.temp).to.be.equal('Zuko');
          expect(res.text).to.be.equal(`Earth, Fire, Water, Air`);
        }));
  });

  function parentPathRouteSetup() {
    const elementsRoute = new RouteBuilder()
        .setPath('/elements')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Earth, Fire, Water, Air');
        })
        .build();

    const protagonistRoute = new RouteBuilder()
        .setPath('/protagonist')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Aang, Toph, Katara, Sokka, Zuko');
        })
        .build();

    const antagonistRoute = new RouteBuilder()
        .setPath('/antagonist')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Firelord Ozai, Azula');
        })
        .build();

    server.addParentPathRoutes('/avatar',
        [elementsRoute, protagonistRoute], antagonistRoute);
    server.buildMiddleware();
  }

  test('will successed on multiple parent route calls', async () => {
    parentPathRouteSetup();
    await supertest(server.app)
        .get(`/avatar/elements`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Earth, Fire, Water, Air`);
        }));

    await supertest(server.app)
        .get(`/avatar/protagonist`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Aang, Toph, Katara, Sokka, Zuko`);
        }));

    await supertest(server.app)
        .get(`/avatar/antagonist`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Firelord Ozai, Azula`);
        }));
  });

  function multipleRoutesSetup() {
    const elementsRoute = new RouteBuilder()
        .setPath('/elements')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Earth, Fire, Water, Air');
        })
        .build();

    const protagonistRoute = new RouteBuilder()
        .setPath('/protagonist')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Aang, Toph, Katara, Sokka, Zuko');
        })
        .build();

    const antagonistRoute = new RouteBuilder()
        .setPath('/antagonist')
        .setMethod(methods.GET)
        .addController((req, res) => {
          res.send('Firelord Ozai, Azula');
        })
        .build();

    server.addRoutes([elementsRoute, protagonistRoute], antagonistRoute);
    server.buildMiddleware();
  }

  test('will successed on multiple route calls', async () => {
    multipleRoutesSetup();
    await supertest(server.app)
        .get(`/elements`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Earth, Fire, Water, Air`);
        }));

    await supertest(server.app)
        .get(`/protagonist`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Aang, Toph, Katara, Sokka, Zuko`);
        }));

    await supertest(server.app)
        .get(`/antagonist`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Firelord Ozai, Azula`);
        }));
  });

  function builderPatternServerSetup() {
    server.temp = 'Bumi';

    const route = new RouteBuilder()
        .setPath('/elements')
        .setMethod(methods.GET)
        .addController((req, res, next) => {
          res.send('Earth, Fire, Water, Air');
          next();
        })
        .build();

    server
        .addPreHandlers((req, res, next) => {
          res.setHeader('name', 'Sokka');
          next();
        })
        .addRoutes(route)
        .addPostHandlers((req, res, next) => {
          server.temp = 'Zuko';
        });
    server.buildMiddleware();
  }

  test('will test builder pattern setup', async () => {
    builderPatternServerSetup();
    await supertest(server.app)
        .get(`/elements`)
        .expect(200)
        .then(((res) => {
          expect(res.text).to.be.equal(`Earth, Fire, Water, Air`);
          expect(res.headers.name).to.be.equal(`Sokka`);
          expect(server.temp).to.be.equal('Zuko');
        }));
  });

  test('router builder failure test cases', async () => {
    const routeWithNoPath = new RouteBuilder()
        .setMethod(methods.GET)
        .addController((req, res, next) => {
          res.send('Earth, Fire, Water, Air');
          next();
        });
    expect(routeWithNoPath.build.bind(routeWithNoPath)).to.throw();

    const routeWithNoMethod = new RouteBuilder()
        .setPath('/elements')
        .addController((req, res, next) => {
          res.send('Earth, Fire, Water, Air');
          next();
        });
    expect(routeWithNoMethod.build.bind(routeWithNoMethod)).to.throw();

    const routeWithNoController = new RouteBuilder()
        .setPath('/elements');
    expect(routeWithNoController.build.bind(routeWithNoController)).to.throw();

    const routeWithInvalidMethod = new RouteBuilder()
        .setMethod('methd')
        .addController((req, res, next) => {
          res.send('Earth, Fire, Water, Air');
          next();
        });
    expect(routeWithInvalidMethod.build.bind(routeWithInvalidMethod))
        .to.throw();
  });
});
