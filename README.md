# Express Quick Server

When developing an API, we need to have a basic configuration setup for the API, i.e., logging, error handling, input validation, etc. These are common across all our APIs, and also, these are some repetitive work for every API. 

ExpressQuickServer helps your service by setting up a standard around the API you build.

## How to use?
Add this package under dependencies.
```
npm install @arzooo/express-quick-server --save
```

Check `examples` folder for how to start express server.
For more use cases like using post/pre handlers, using multiple input validators, using multiple controllers & throwing custom error with status code, checkout unit tests. 

## TODO:

1. Implement logging.
1. Caputure API metrics