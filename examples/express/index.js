import { methods, Server, buildRoute, inputValidators, HttpError } from 'express-quick-server'
import joi from 'joi'

const port = 3000
const server = new Server(port);

// Route 1

const bodyValidator = joi.object({
  name: joi.string().required()
})

const paramValidator = joi.object({
  id: joi.string().required()
})

const middleware = (req, res, next) => {
  console.log(`Printing this message from handler 1 id:${req.params.id} and name: ${req.body.name}`)
  next()
}

const controller = (req, res) => {
  console.log(`recieved id:${req.params.id} and name: ${req.body.name}`)
  res.send(`Hello Mr.${req.body.name}`)
}

const inputValidators = {}
inputValidators[inputValidators.PARAMS] = paramValidator
inputValidators[inputValidators.BODY] = bodyValidator

const postRoute = buildRoute('/user/:id', methods.POST, inputValidators, middleware, controller)

// Route 2

// Get controller
const queryValidator = joi.object({
  name: joi.string().required()
})

const routeTwoController = (req, res) => {
  throw HttpError("Infused error", 503)
  res.send("From server")
}

const routeTwoValidators = {}
routeTwoValidators[inputValidators.QUERY] = queryValidator

const getRoute = buildRoute('/order', methods.GET, routeTwoValidators, routeTwoController)


// Adding to routers to server

server.addRoutes(getRoute, postRoute)
server.start()