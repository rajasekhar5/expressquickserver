'use strict';

import {inputValidators} from './inputValidators';
import {methods} from './methods';
import {buildRoute} from './iRoute';

export class RouteBuilder {
  #validators;
  #controller;
  #path;
  #method;

  constructor() {
    this.#validators = {};
    this.#method = undefined;
    this.#path = undefined;
    this.#controller = [];
  }

  setPath(path) {
    this.#path = path;
    return this;
  }

  setMethod(method) {
    this.#method = method;
    return this;
  }

  addController(controller) {
    this.#controller.push(controller);
    return this;
  }

  setParamsValidator(validator) {
    this.#validators[inputValidators.PARAMS] = validator;
    return this;
  }

  setBodyValidator(validator) {
    this.#validators[inputValidators.BODY] = validator;
    return this;
  }

  setFieldsValidator(validator) {
    this.#validators[inputValidators.FIELDS] = validator;
    return this;
  }

  setHeadersValidator(validator) {
    this.#validators[inputValidators.HEADERS] = validator;
    return this;
  }

  setQueryValidator(validator) {
    this.#validators[inputValidators.QUERY] = validator;
    return this;
  }

  #isInvalidRoute() {
    return !methods.hasOwnProperty(this.#method) ||
        this.#controller.length === 0 || this.#path === undefined;
  }

  build() {
    if (this.#isInvalidRoute()) {
      throw new Error(`Missing method/controller/path in route.`);
    }

    return buildRoute(
        this.#path,
        this.#method,
        this.#validators,
        this.#controller,
    );
  }
};
