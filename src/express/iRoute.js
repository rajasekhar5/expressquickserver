'use strict';

const iRoute = {
  path: undefined,
  method: undefined,
  inputValidators: undefined,
  controllers: undefined,
};

const buildRoute = (path, method, inputValidators, ...controllers) => {
  const route = Object.create(iRoute);
  route.path = path;
  route.method = method;
  route.inputValidators = inputValidators;
  route.controllers = controllers;
  return route;
};

export {
  buildRoute,
};
