'use strict';

import 'core-js/stable';
import 'regenerator-runtime/runtime';
import responseTime from 'response-time';
import createError from 'http-errors';
import express from 'express';
import {json, urlencoded} from 'express';
import {createValidator} from 'express-joi-validation';
import {inputValidators} from './inputValidators';

class Server {
  #validator;
  #preHandlers;
  #routes;
  #postHandlers;
  #port;
  #logger;

  constructor(port, logger = console) {
    this.app = express();
    this.#port = port || 8080;
    this.#validator = createValidator({});
    this.#preHandlers = [];
    this.#routes = [];
    this.#postHandlers = [];
    this.#logger = logger;
  }

  #makeHandlerAwareOfAsyncErrors(handler) {
    return async function(req, res, next) {
      try {
        await handler(req, res, next);
      } catch (error) {
        next(error);
      }
    };
  }

  // TODO(Raj): Implement error logging.
  #errorHandler(err, req, res, next) {
    err.status = err.status || 500;
    res.status(err.status).json({status: err.status, message: err.toString()});
  }

  #defaultPreHandlers() {
    this.app.use(json());
    this.app.use(urlencoded({extended: true}));
    this.app.use(responseTime());
  }

  #defaultPostHandlers() {
    this.app.use(function(req, res, next) {
      next(createError(404));
    });

    this.app.use(this.#errorHandler);
  }

  #buildPreHandlers() {
    this.#defaultPreHandlers();
    this.#preHandlers.flat().forEach((handler) => {
      this.app.use(handler);
    });
  }

  #buildPostHandlers() {
    this.#postHandlers.flat().forEach((handler) => {
      this.app.use(handler);
    });
    this.#defaultPostHandlers();
  }

  buildMiddleware() {
    this.#buildPreHandlers();
    this.#buildRoutes();
    this.#buildPostHandlers();
  }

  start() {
    this.buildMiddleware();

    this.app.listen(this.#port, () => {
      this.#logger.info(`App listening at http://localhost:${this.#port}`);
    });
  }

  addPreHandlers(...handlers) {
    this.#preHandlers.push(handlers.flat());
    return this;
  }

  addPostHandlers(...handlers) {
    this.#postHandlers.push(handlers.flat());
    return this;
  }

  #generateValidatorsArray(requestValidators) {
    const result = [];
    if (requestValidators === null ||
        requestValidators === undefined ||
        Object.keys(requestValidators).length === 0) {
      return result;
    }

    if (inputValidators.HEADERS in requestValidators) {
      result.push(
          this.#validator.headers(requestValidators[inputValidators.HEADERS]));
    }
    if (inputValidators.PARAMS in requestValidators) {
      result.push(
          this.#validator.params(requestValidators[inputValidators.PARAMS]));
    }
    if (inputValidators.QUERY in requestValidators) {
      result.push(
          this.#validator.query(requestValidators[inputValidators.QUERY]));
    }
    if (inputValidators.FIELDS in requestValidators) {
      result.push(
          this.#validator.fields(requestValidators[inputValidators.FIELDS]));
    }
    if (inputValidators.BODY in requestValidators) {
      result.push(
          this.#validator.body(requestValidators[inputValidators.BODY]));
    }

    return result;
  }

  addRoutes(...routes) {
    this.#routes.push(routes.flat());
    return this;
  }

  #deafultRoutes() {
    // Use for service health checks.
    this.app.get('/ping', function(req, res) {
      res.send('healthy');
    });
  }

  addParentPathRoutes(parentPath, ...controllers) {
    this.#routes.push(controllers.flat().flatMap((controller) => {
      controller.path = parentPath + controller.path;
      return controller;
    }));
    return this;
  }

  #generateListOfControllers(controllers) {
    const result = [];
    controllers.flat().forEach((controller) => {
      result.push(this.#makeHandlerAwareOfAsyncErrors(controller));
    });

    return result;
  }

  #buildRoutes() {
    this.#deafultRoutes();
    this.#routes.flat().forEach((route) => {
      const listOfControllers =
        this.#generateListOfControllers(route.controllers);
      const listOfInputValidators =
        this.#generateValidatorsArray(route.inputValidators);
      this.app[route.method.toLowerCase()](route.path,
          listOfInputValidators, listOfControllers);
    });
  }
}

export {
  Server,
};
