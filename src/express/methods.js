'use strict';

const methods = Object.freeze({
  DELETE: 'DELETE',
  GET: 'GET',
  PATCH: 'PATCH',
  POST: 'POST',
  PUT: 'PUT',
});

export {
  methods,
};
