'use strict';

class CustomError extends Error {
  constructor(message, status) {
    super();
    Error.captureStackTrace( this, this.constructor );
    this.name = 'CustomError';
    this.message = message;
    if (status) this.status = status;
  }
}

export {
  CustomError,
  CustomError as HttpError,
};
