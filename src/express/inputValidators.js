'use strict';

const inputValidators = Object.freeze({
  BODY: 'BODY',
  FIELDS: 'FIELDS',
  HEADERS: 'HEADERS',
  PARAMS: 'PARAMS',
  QUERY: 'QUERY',
});

export {
  inputValidators,
};
